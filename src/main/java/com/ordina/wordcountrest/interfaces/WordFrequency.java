package com.ordina.wordcountrest.interfaces;

/**
 * @author Laurens Wes
 * WordFrequency interface as provided in the assessment
 */
public interface WordFrequency {
	String getWord();
	int getFrequency();
}
