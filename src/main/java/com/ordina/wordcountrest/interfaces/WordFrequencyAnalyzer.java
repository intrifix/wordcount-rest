package com.ordina.wordcountrest.interfaces;

import java.util.List;

/**
 * @author Laurens Wes
 * WordFrequencyAnalyzer interface as provided in the assessment
 */
public interface WordFrequencyAnalyzer {
	int calculateHighestFrequency(String text);
	int calculateFrequencyForWord (String text, String word);
	List<WordFrequency> calculateMostFrequentNWords (String text, int n);
}
