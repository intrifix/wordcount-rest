package com.ordina.wordcountrest.endpoints;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.ordina.wordcountrest.FrequencyAnalyzer;

/**
 * Servlet implementation class CalculateFrequencyForWord
 */
public class CalculateFrequencyForWord extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculateFrequencyForWord() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Initialize the FrequencyAnalyzer with custom delimiters if provided
		String delimiters = request.getParameter("delimiters");
		FrequencyAnalyzer analyzer = delimiters == null ? 
				new FrequencyAnalyzer() : 
				new FrequencyAnalyzer(delimiters);
		
		String text = request.getParameter("text");
		String word = request.getParameter("word");
		if (text == null) {
			response.getWriter().append("Parameter 'text' was missing");
		} else if (word == null) {
			response.getWriter().append("Parameter 'word' was missing");
		} else {
			response.getWriter().append(String.valueOf(analyzer.calculateFrequencyForWord(text, word)));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
