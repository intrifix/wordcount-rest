package com.ordina.wordcountrest;

import com.ordina.wordcountrest.interfaces.WordFrequency;

/**
 * @author Laurens Wes
 * Frequency class that keeps track of the frequency of a given word,
 * also implements the Comparable<Frequency> interface used when
 * sorting 
 */
public class Frequency implements WordFrequency, Comparable<Frequency> {
	
	private String word;
	private int frequency;
	
	public Frequency(String word, int frequency) {
		setWord(word);
		setFrequency(frequency);
	}
	
	/**
	 * Sets the word (private, only used in constructor)
	 * @param word
	 */
	private void setWord(String word) {
		this.word = word;
	}
	
	@Override
	public String getWord() {
		return word;
	}
	
	/**
	 * Sets the frequency (only used in constructor)
	 * @param frequency
	 */
	private void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	/**
	 * Increments the frequency of the word by 1
	 */
	public void incrementFrequency() {
		this.frequency++;
	}
	
	@Override
	public int getFrequency() {
		return frequency;
	}
	
	/**
	 * toString method to display the result
	 */
	public String toString() {
		return "(\"" + word + "\", " + frequency + ")";
	}

	/**
	 * Compares this word frequency to a given other word frequency. This function
	 * first sorts by frequency (from high to low) and if the frequencies are the same this
	 * function sorts alphabetically (ascending).
	 */
	@Override
	public int compareTo(Frequency o) {
		if (Integer.compare(o.getFrequency(), frequency) == 0) {
			return word.compareTo(o.getWord());
		} else {
			return Integer.compare(o.getFrequency(), frequency);
		}
	}
}
