package com.ordina.wordcountrest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.ordina.wordcountrest.interfaces.WordFrequency;
import com.ordina.wordcountrest.interfaces.WordFrequencyAnalyzer;

/**
 * @author Laurens Wes
 * Class to analyze the frequency of words in a specific text. Supports 3 main methods
 * as defined in the interface WordFrequencyAnalyzer.
 */
public class FrequencyAnalyzer implements WordFrequencyAnalyzer {
	
	// The default delimiters (1 or more of the following characters: {whitespace, dot, comma, dash})
	private static final String DEFAULT_DELIMITERS = "([\\s\\.,\\-])+";
	private String delimiters;
	
	/**
	 * Default constructor, which makes this instance use the default delimiters
	 */
	public FrequencyAnalyzer() {
		setDelimiters(DEFAULT_DELIMITERS);
	}
	
	/**
	 * Constructor that enables the user to provide a regex to specify delimiters
	 * @param delimiters
	 */
	public FrequencyAnalyzer(String delimiters) {
		this.setDelimiters(delimiters);
	}
	
	@Override
	public int calculateHighestFrequency(String text) {
		HashMap<String, Frequency> frequencies = getFrequenciesFromText(text);
		int highestFrequency = 0;
		for (Entry<String, Frequency> s : frequencies.entrySet()) {
			if (s.getValue().getFrequency() > highestFrequency)
				highestFrequency = s.getValue().getFrequency();
		}
		return highestFrequency;
	}
	
	/**
	 * Returns the frequency of a given word in a text. If the word does not exist within
	 * the text, this function returns 0.
	 */
	@Override
	public int calculateFrequencyForWord(String text, String word) {
		HashMap<String, Frequency> frequencies = getFrequenciesFromText(text);
		if (frequencies.containsKey(word.toLowerCase())) {
			return frequencies.get(word.toLowerCase()).getFrequency();
		} else {
			return 0;
		}
	}
	
	/**
	 * Gets all word frequencies from the text, then sorts the resulting list.
	 * When n is larger than the number of unique words, the number of unique words is used
	 * instead to return the most frequently occuring words.
	 */
	@Override
	public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
		HashMap<String, Frequency> frequencies = getFrequenciesFromText(text);
		List<Frequency> allFrequencies = new ArrayList<>();
		frequencies.forEach((word, frequency) -> allFrequencies.add(frequency));
		Collections.sort(allFrequencies);
		
		// Return only the top n words (or all the words if n > <unique words>)
		List<WordFrequency> output = new ArrayList<>();
		for (int i = 0; i < (n > allFrequencies.size() ? allFrequencies.size() : n); i++)
			output.add(allFrequencies.get(i));
		return output;
	}
	
	/**
	 * Gets all frequencies of all unique words occuring in the text by first splitting
	 * the text into a list given the delimiter regex, then iterates over the list and creates
	 * a hashmap containing all words and their frequencies.
	 * @param text	the text to process
	 * @return	a hashmap of words and their WordFrequency objects
	 */
	private HashMap<String, Frequency> getFrequenciesFromText(String text) {
		String[] noDelimiters = text.toLowerCase().split(delimiters);
		HashMap<String, Frequency> frequencies = new HashMap<>();
		for (String s : noDelimiters) {
			if (s.matches("[a-zA-Z]+")) {
				if (!frequencies.containsKey(s)) {
					frequencies.put(s, new Frequency(s, 1));
				} else {
					frequencies.get(s).incrementFrequency();
				}
			}
		}
		return frequencies;
	}
	
	/**
	 * Returns the delimiter regex used
	 * @return regex
	 */
	public String getDelimiters() {
		return delimiters;
	}

	/**
	 * Sets the delimiter regex
	 * @param delimiters as regex
	 */
	public void setDelimiters(String delimiters) {
		this.delimiters = delimiters;
	}
	
}
