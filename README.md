# Ordina - Word Count



## Name
Word Count assessment (REST)

## Description
This project is part of a text processing library. It can return the word(s) in a text that occur most often, the specific frequency for a given word and the most frequently occurring n words.

Developed using Eclipse 2022-04 and Java 17.

## Installation
The project can be opened in Eclipse EE and deployed on -for example- a TomCat server. Alternatively, the included .war file can be deployed on a server.

## Usage

The following endpoints are available with their respective required and optional parameters:
-	/CalculateHighestFrequency
		Required parameters: text
		Optional parameters: delimiters
-	/CalculateFrequencyForWord
		Required parameters: text, word
		Optional parameters: delimiters
-	/CalculateMostFrequentNWords
		Required parameters: text, n
		Optional parameters: delimiters
	
A PostMan collection is included with examples on how to access the REST service.

One example, assuming the file WordCount_REST.war is deployed locally on port 8080:
http://localhost:8080/WordCount_REST/CalculateMostFrequentNWords?text=The%20sun%20shines%20over%20the%20lake&n=3

result: [("the", 2), ("lake", 1), ("over", 1)]
